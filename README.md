Le projet fil rouge support au TP est une application de gestion d’évènements sportifs de type running permettant

- de déclarer un évènement organisé par un partenaire
- de notifier les abonnés à un partenaire lors de l’apparition d’un nouvel évènement

L’application fonctionne en ligne de commande et s’appuie sur une architecture 3-tiers

- présentation (CLI)
- service (logique métier)
- données

L’application peut s’exécuter avec 2 types de système de gestion de données

- fichiers
- SGBD

Le choix du système de gestion de données est géré par l’intermédiaire du fichier repositoryFactory.properties et la mise en oeuvre du Design Pattern Factory.