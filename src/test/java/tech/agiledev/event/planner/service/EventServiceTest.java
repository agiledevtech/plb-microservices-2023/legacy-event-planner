package tech.agiledev.event.planner.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import tech.agiledev.event.planner.dao.EventRepository;
import tech.agiledev.event.planner.dao.SubscriberRepository;
import tech.agiledev.event.planner.model.Event;
import tech.agiledev.event.planner.model.Subscriber;
import tech.agiledev.event.planner.util.MailUtil;

@ExtendWith(MockitoExtension.class)
class EventServiceTest {

	@Mock
	EventRepository eventRepository;
	
	@Mock
	SubscriberRepository subscriberRepository;
	
	@Mock
	MailUtil mailUtil;
	
	@Test
	void createEvent_for_a_partner_without_subscribers_should_not_send_any_mail() throws IOException {
		EventService eventService = new EventService();
		eventService.setEventRepository(eventRepository);
		eventService.setSubscriberRepository(subscriberRepository);
		eventService.setMailUtil(mailUtil);
		Event event = new Event(LocalDate.now(), "Chamonix", 90);
		Event storedEvent = new Event(1L, LocalDate.now(), "Chamonix", 90);
		String partner = "5ème Elément";
		when(eventRepository.createEventForPartner(event, partner)).thenReturn(storedEvent);
		ArrayList<Subscriber> emptyList = new ArrayList<>();
		when(subscriberRepository.getAllForPartner(partner)).thenReturn(emptyList);
		
		eventService.createEvent(partner, event);
		
		verify(eventRepository).createEventForPartner(event, partner);
		verify(subscriberRepository).getAllForPartner(partner);
		verify(mailUtil).informNewEventCreation(emptyList, storedEvent);
	}

}
