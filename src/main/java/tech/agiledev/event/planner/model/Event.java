package tech.agiledev.event.planner.model;

import java.time.LocalDate;

public record Event(Long id, LocalDate when, String where, Integer distanceKm) {

	public Event(LocalDate when, String where, Integer distanceKm) {
		this(null, when, where, distanceKm);
	}
}
