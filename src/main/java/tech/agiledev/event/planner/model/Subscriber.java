package tech.agiledev.event.planner.model;

public record Subscriber(Long id, String firstName, String lastName, String email) {
	
}
