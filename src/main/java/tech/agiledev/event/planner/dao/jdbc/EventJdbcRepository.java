package tech.agiledev.event.planner.dao.jdbc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tech.agiledev.event.planner.dao.EventRepository;
import tech.agiledev.event.planner.model.Event;

/**
 * CREATE TABLE EVENT (ID BIGINT auto_increment, PARTNER VARCHAR(25), OCCURS_WHEN DATE, LOCATION VARCHAR(25), DISTANCE_KM INTEGER, PROMOTED BOOLEAN);
 * INSERT INTO EVENT (PARTNER, OCCURS_WHEN, LOCATION, DISTANCE_KM, PROMOTED) VALUES ('Globetrailers', parsedatetime('02/05/2019','dd/MM/yyyy'), 'Caroux',84,TRUE);
 */
public class EventJdbcRepository extends JdbcRepository implements EventRepository {
	
	private static Logger LOGGER = LoggerFactory.getLogger(EventJdbcRepository.class);
	
	public EventJdbcRepository(String dbUrl) {
		super(dbUrl);
	}

	@Override
	public Event createEventForPartner(Event event, String partner) throws IOException {
		ResultSet rs = null;
		try (
				Connection conn = getConnection();
				PreparedStatement ps = conn.prepareStatement(
						"INSERT INTO EVENT (PARTNER, OCCURS_WHEN, LOCATION, DISTANCE_KM, PROMOTED) VALUES (?,?,?,?,?)",
						Statement.RETURN_GENERATED_KEYS
				);
		) {
			ps.setString(1, partner);
			ps.setObject(2, event.when());
			ps.setString(3, event.where());
			ps.setInt(4, event.distanceKm());
			ps.setBoolean(5, false);
			ps.execute();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				event = new Event(rs.getLong(1), event.when(), event.where(), event.distanceKm());
			}
			LOGGER.debug("Created a new event : {}", event);
			return event;
		} catch (SQLException e) {
			throw new IOException(e.getMessage());
		} finally {
			closeResultSet(rs);
		}
	}

	@Override
	public void promoteEvent(Long id) throws IOException {
		LOGGER.debug("Going to promote event {}", id);
		try (
				Connection conn = getConnection();
				PreparedStatement ps = conn.prepareStatement("UPDATE EVENT SET PROMOTED = ? WHERE ID = ?");
		) {
			ps.setBoolean(1, true);
			ps.setLong(2, id);
			int amount = ps.executeUpdate();
			if (amount == 1) {
				LOGGER.debug("Promoted event : {}", id);
			}
		} catch (SQLException e) {
			throw new IOException(e.getMessage());
		}
	}
}
