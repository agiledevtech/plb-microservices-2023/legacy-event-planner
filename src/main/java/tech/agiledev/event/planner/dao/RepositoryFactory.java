package tech.agiledev.event.planner.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tech.agiledev.event.planner.dao.file.EventFileRepository;
import tech.agiledev.event.planner.dao.file.SubscriberFileRepository;
import tech.agiledev.event.planner.dao.jdbc.EventJdbcRepository;
import tech.agiledev.event.planner.dao.jdbc.SubscriberJdbcRepository;

public class RepositoryFactory {

	private static Logger LOGGER = LoggerFactory.getLogger(RepositoryFactory.class);

	private static final String TYPE_JDBC = "JDBC";
	private static final String TYPE_FILE = "FILE";

	private static final RepositoryFactory singleton = new RepositoryFactory();

	private String fileRepositoriesSubdirectory;
	private String dbRelativeUrl;
	private String type;

	private RepositoryFactory() {
		super();
		final Properties properties = new Properties();
		try (final InputStream stream = this.getClass().getResourceAsStream("/repositoryFactory.properties")) {
			properties.load(stream);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}

		this.type = properties.getProperty("repositories.type");
		LOGGER.debug("Configuring repository of type {}", this.type);
		switch (this.type) {
			case TYPE_FILE -> this.fileRepositoriesSubdirectory = "/src/main/data";
			case TYPE_JDBC -> this.dbRelativeUrl = "/src/main/db/eventPlanner";
			default -> throw new RuntimeException("Unknown repositories configuration : " + this.type);
		}
	}

	public static RepositoryFactory getInstance() {
		return singleton;
	}

	public EventRepository getEventRepository() {
		return switch (this.type) {
			case TYPE_FILE -> new EventFileRepository(this.fileRepositoriesSubdirectory);
			case TYPE_JDBC -> new EventJdbcRepository(this.dbRelativeUrl);
			default -> null;
		};
	}

	public SubscriberRepository getSubscriberRepository() {
		return switch (this.type) {
			case TYPE_FILE -> new SubscriberFileRepository(this.fileRepositoriesSubdirectory);
			case TYPE_JDBC -> new SubscriberJdbcRepository(this.dbRelativeUrl);
			default -> null;
		};
	}

}
