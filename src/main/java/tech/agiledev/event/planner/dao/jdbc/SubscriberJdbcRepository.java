package tech.agiledev.event.planner.dao.jdbc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tech.agiledev.event.planner.dao.SubscriberRepository;
import tech.agiledev.event.planner.model.Subscriber;

/**
 * CREATE TABLE SUBSCRIBER (ID BIGINT auto_increment, PARTNER VARCHAR(25), FIRST_NAME VARCHAR(25), LAST_NAME VARCHAR(25), EMAIL VARCHAR(100));
 * INSERT INTO SUBSCRIBER (PARTNER, FIRST_NAME, LAST_NAME, EMAIL) VALUES ('Globetrailers', 'Eric', 'SIBER', 'eric@agiledev.tech');
 */
public class SubscriberJdbcRepository extends JdbcRepository implements SubscriberRepository {

    private static Logger LOGGER = LoggerFactory.getLogger(SubscriberJdbcRepository.class);

    public SubscriberJdbcRepository(String dbUrl) {
        super(dbUrl);
    }

    @Override
    public List<Subscriber> getAllForPartner(String partner) throws IOException {
        LOGGER.debug("Loading all the subscribers for partner {}", partner);
        List<Subscriber> result = new ArrayList<>();
        ResultSet rs = null;
        try (
                Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement("SELECT * FROM SUBSCRIBER WHERE PARTNER = ?");
        ) {
            ps.setString(1, partner);
            rs = ps.executeQuery();
            while (rs.next()) {
                Subscriber subscriber = new Subscriber(rs.getLong("ID"),
                        rs.getString("FIRST_NAME"),
                        rs.getString("LAST_NAME"),
                        rs.getString("EMAIL"));
                result.add(subscriber);
            }
            return result;
        } catch (SQLException e) {
            throw new IOException(e.getMessage());
        } finally {
            closeResultSet(rs);
        }
    }
}
