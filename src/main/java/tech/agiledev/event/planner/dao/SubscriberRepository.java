package tech.agiledev.event.planner.dao;

import java.io.IOException;
import java.util.List;

import tech.agiledev.event.planner.model.Subscriber;

public interface SubscriberRepository {

	List<Subscriber> getAllForPartner(String partner) throws IOException;

}
