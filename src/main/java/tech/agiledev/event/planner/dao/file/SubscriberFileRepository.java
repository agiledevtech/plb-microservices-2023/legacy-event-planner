package tech.agiledev.event.planner.dao.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tech.agiledev.event.planner.dao.SubscriberRepository;
import tech.agiledev.event.planner.model.Subscriber;

/**
 * Subscribers data file is expecting to contain lines as PARTNER_NAME;FIRST_NAME;LAST_NAME;EMAIL
 *
 */
public class SubscriberFileRepository implements SubscriberRepository {

	private static Logger LOGGER = LoggerFactory.getLogger(SubscriberFileRepository.class);
	
	private static final String SUBSCRIBERS_FILE = "subscribers.txt";
	
	private File repositoryDirectory;

	public SubscriberFileRepository(String fileRepositoriesSubdirectory) {
		super();
		String projectDir = System.getProperty("user.dir");
		this.repositoryDirectory = new File(projectDir, fileRepositoriesSubdirectory);
		LOGGER.debug("Configuring File repository for URL {}", this.repositoryDirectory.getAbsolutePath());
	}

	@Override
	public List<Subscriber> getAllForPartner(String partner) throws IOException {
		LOGGER.debug("Loading all the subscribers for partner {}", partner);
		File subscribersFile = new File(repositoryDirectory, SUBSCRIBERS_FILE);
		try (Stream<String> stream = Files.lines(Paths.get(subscribersFile.toURI()))) {
			return stream.filter(line -> line.startsWith(partner))
				.map(this::buildSubscriber)
				.collect(Collectors.toList());
		}
	}
	
	private Subscriber buildSubscriber(String item) {
		String[] parts = item.split(";");
		Subscriber subscriber = new Subscriber(Long.valueOf(parts[1]), parts[2], parts[3], parts[4]);
		return subscriber;
	}
	
}
