package tech.agiledev.event.planner.dao;

import java.io.IOException;

import tech.agiledev.event.planner.model.Event;

public interface EventRepository {

	Event createEventForPartner(Event event, String partner) throws IOException;
	
	void promoteEvent(Long id) throws IOException;

}
