package tech.agiledev.event.planner.dao.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tech.agiledev.event.planner.dao.EventRepository;
import tech.agiledev.event.planner.model.Event;

/**
 * Events data file is expecting to contain lines as
 * PARTNER;WHERE;DISTANCE;WHEN;PROMOTED Event id is built from the position
 * inside the file. The WHEN data is formatted as DD/MM/YYYY inside the file.
 * The PROMOTED data is a boolean (true or false inside the file)
 *
 */
public class EventFileRepository implements EventRepository {

	private static Logger LOGGER = LoggerFactory.getLogger(EventFileRepository.class);
	
	private static final String EVENTS_FILE = "events.txt";

	private File repositoryDirectory;

	public EventFileRepository(String fileRepositoriesSubdirectory) {
		super();
		String projectDir = System.getProperty("user.dir");
		this.repositoryDirectory = new File(projectDir, fileRepositoriesSubdirectory);
		LOGGER.debug("Configuring File repository for URL {}", this.repositoryDirectory.getAbsolutePath());
	}

	@Override
	public Event createEventForPartner(Event event, String partner) throws IOException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		File eventsFile = new File(repositoryDirectory, EVENTS_FILE);
		long nextId = -1;
		try (Stream<String> stream = Files.lines(Paths.get(eventsFile.toURI()))) {
			nextId = 1 + stream.count();
		}
		try (
				FileWriter fw = new FileWriter(eventsFile, true);
				BufferedWriter bw = new BufferedWriter(fw)
		) {
			bw.newLine();
			bw.write(String.format("%s;%s;%s;%s;false", partner, event.where(), event.distanceKm(),
					formatter.format(event.when())));
			event = new Event(nextId, event.when(), event.where(), event.distanceKm());
			LOGGER.debug("Created a new event : {}", event);
			return event;
		}
	}

	@Override
	public void promoteEvent(Long id) throws IOException {
		LOGGER.debug("Going to promote event {}", id);
		File eventsFile = new File(repositoryDirectory, EVENTS_FILE);
		File workEventsFile = new File(repositoryDirectory, EVENTS_FILE + ".tmp");
		try (
				FileReader fr = new FileReader(eventsFile);
				FileWriter fw = new FileWriter(workEventsFile);
				BufferedReader br = new BufferedReader(fr);
				BufferedWriter bw = new BufferedWriter(fw)
		) {
			long currentId = 0;
			String line = br.readLine();
			while (line != null) {
				currentId++;
				if (currentId != 1) {
					bw.newLine();
				}
				if (currentId == id) {
					LOGGER.debug("Promoted event : {}", line);
					line = line.replace(";false", ";true");
				} 
				bw.write(line);
				line = br.readLine();
			}
			eventsFile.delete();
			workEventsFile.renameTo(eventsFile);
		}
	}

}
