package tech.agiledev.event.planner.dao.jdbc;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class JdbcRepository {
	
	private static Logger LOGGER = LoggerFactory.getLogger(JdbcRepository.class);
	
	protected String dbUrl;
	
	public JdbcRepository(String dbRelativeUrl) {
		super();
		String projectDir = System.getProperty("user.dir");
		String path = new File(projectDir).getAbsolutePath();
		this.dbUrl = "jdbc:h2:tcp://localhost/" + path + dbRelativeUrl;
		LOGGER.debug("Configuring DB for URL {}", this.dbUrl);
	}

	protected Connection getConnection() throws SQLException {
		return DriverManager.getConnection(this.dbUrl, "sa", "");
	}

	protected void closeResultSet(ResultSet rs) throws IOException {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e1) {
				throw new IOException(e1.getMessage());
			}
		}
	}
}
