package tech.agiledev.event.planner.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import tech.agiledev.event.planner.dao.EventRepository;
import tech.agiledev.event.planner.dao.SubscriberRepository;
import tech.agiledev.event.planner.model.Event;
import tech.agiledev.event.planner.model.Subscriber;
import tech.agiledev.event.planner.util.MailUtil;

public class EventService {

	private EventRepository eventRepository;
	private SubscriberRepository subscriberRepository;
	private MailUtil mailUtil;
	private List<String> promotedPartners;

	public EventService() {
		super();
		promotedPartners = Arrays.asList("Globetrailers");
	}

	public void createEvent(String partner, Event event) throws IOException {
		event = this.eventRepository.createEventForPartner(event, partner);
		List<Subscriber> subscribers = this.subscriberRepository.getAllForPartner(partner);
		this.mailUtil.informNewEventCreation(subscribers, event);
		if (this.promotedPartners.contains(partner)) {
			this.eventRepository.promoteEvent(event.id());
		}
	}

	public void setEventRepository(EventRepository eventRepository) {
		this.eventRepository = eventRepository;
	}

	public void setSubscriberRepository(SubscriberRepository subscriberRepository) {
		this.subscriberRepository = subscriberRepository;
	}

	public void setMailUtil(MailUtil mailUtil) {
		this.mailUtil = mailUtil;
	}
}
