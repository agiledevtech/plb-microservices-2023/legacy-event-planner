package tech.agiledev.event.planner.service;

import tech.agiledev.event.planner.dao.RepositoryFactory;
import tech.agiledev.event.planner.util.MailUtil;

public class ServiceFactory {

	private static final ServiceFactory singleton = new ServiceFactory();
	
	private ServiceFactory() {
		super();
	}
	
	public static ServiceFactory getInstance() {
		return singleton;
	}
	
	public EventService getEventService() {
		EventService eventService = new EventService();
		eventService.setEventRepository(RepositoryFactory.getInstance().getEventRepository());
		eventService.setSubscriberRepository(RepositoryFactory.getInstance().getSubscriberRepository());
		eventService.setMailUtil(new MailUtil());
		return eventService;
	}
}
