package tech.agiledev.event.planner.util;

import java.util.List;
import java.util.Properties;

import jakarta.mail.Authenticator;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.PasswordAuthentication;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tech.agiledev.event.planner.model.Event;
import tech.agiledev.event.planner.model.Subscriber;

public class MailUtil {

	private static Logger LOGGER = LoggerFactory.getLogger(MailUtil.class);

	private static final String SENDER_EMAIL = "noreply@agiledev.tech";
	private static final String EVENT_CREATED_SUBJECT = "New event creation notification";
	private static final String EVENT_CREATED_TEMPLATE = "Hi,<br/><br/>An event of %d km has been planned in %s at %td/%tm/%tY by an organizer you follow.";

	private String username;
	private String password;
	private Properties props;

	public MailUtil() {
		Properties properties = System.getProperties();
		properties.put("mail.smtp.host", "smtp.ethereal.email");
		properties.put("mail.smtp.port", "587");
		properties.setProperty("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable","true");
		
		this.props = properties;
		this.username = "vickie.russel57@ethereal.email";
		this.password = "TJjXYTV9MHBtCVzSFz";
	}

	public void informNewEventCreation(List<Subscriber> subscribers, Event event) {
		Session session = Session.getInstance(this.props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		LOGGER.debug("Notifying {} subscribers for new event {}", subscribers.size(), event.id());
		for (Subscriber subscriber : subscribers) {
			try {
				informNewEventCreation(session, subscriber, event);
			} catch (MessagingException e) {
				LOGGER.error("An error occurred when trying to notify {} for new event {} : {}", subscriber.email(),
						event.id(), e.getMessage());
			}
		}
	}

	private void informNewEventCreation(Session session, Subscriber subscriber, Event event) throws MessagingException {
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(SENDER_EMAIL));
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(subscriber.email()));
		message.setSubject(EVENT_CREATED_SUBJECT);

		MimeBodyPart mimeBodyPart = new MimeBodyPart();
		mimeBodyPart.setContent(String.format(EVENT_CREATED_TEMPLATE, event.distanceKm(), event.where(),
				event.when(), event.when(), event.when()), "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(mimeBodyPart);
		message.setContent(multipart);
		LOGGER.debug("Notifying {} for new event {}", subscriber.email(), event.id());
		Transport.send(message);
	}

}
