package tech.agiledev.event.planner;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;

import org.beryx.textio.TextIO;
import org.beryx.textio.TextIoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tech.agiledev.event.planner.model.Event;
import tech.agiledev.event.planner.service.EventService;
import tech.agiledev.event.planner.service.ServiceFactory;

public class EventPlannerCLI {

	private static Logger LOGGER = LoggerFactory.getLogger(EventPlannerCLI.class);

	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	private EventService eventService;
	private TextIO textIO;

	public EventPlannerCLI(EventService eventService, TextIO textIO) {
		this.eventService = eventService;
		this.textIO = textIO;
	}

	public static void main(String[] args) {
		try {
			EventPlannerCLI cli = new EventPlannerCLI(ServiceFactory.getInstance().getEventService(),
					TextIoFactory.getTextIO());
			while (true) {
				cli.interactWithUser();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	private void interactWithUser() {
		textIO.getTextTerminal().println("*************************************");
		String menuAnswer = textIO.newStringInputReader().withPossibleValues("y", "n")
				.read("Do you want to provide a new event ?");
		switch (menuAnswer) {
		case "y":
			try {
				askForAndCreateNewEvent();
				textIO.getTextTerminal().println();
				textIO.getTextTerminal().println("Event has been successfully created.");
			} catch (IOException e) {
				textIO.getTextTerminal().println();
				textIO.getTextTerminal().println("An error occurred : " + e.getMessage());
			} finally {
				textIO.getTextTerminal().println();
			}
			break;
		case "n":
			System.exit(0);
		default:
			break;
		}
	}

	private void askForAndCreateNewEvent() throws IOException {
		String organizer = textIO.newStringInputReader().read("Who is the organizer ?");
		String location = textIO.newStringInputReader().read("Where would it happen ?");
		int distanceKm = textIO.newIntInputReader().read("How many km ?");
		String when = textIO.newStringInputReader().withMinLength(10).withMaxLength(10)
				.withDefaultValue(formatter.format(LocalDate.now()))
				.withValueChecker(EventPlannerCLI::validateDateInput).read("When will it happen ?");
		Event event = new Event(LocalDate.parse(when, formatter), location, distanceKm);
		LOGGER.debug("About to create event {} for partner {}", event, organizer);
		eventService.createEvent(organizer, event);
	}

	private static List<String> validateDateInput(String val, String item) {
		try {
			formatter.parse(val);
			return null;
		} catch (DateTimeParseException e) {
			return Arrays.asList("Provided input is not a date.");
		}
	}

}
